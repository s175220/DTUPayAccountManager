package interfaces;

import code.User;

import java.util.Optional;

public interface UserDatabase {
    void add(User user);

    Optional<User> findUser(String searchTerm);

    void removeUser(String searchTerm);
}
