package database;
import code.*;

import java.util.LinkedList;
import java.util.Optional;

/**
 *
 * @author Mussab
 * UserDatabase: Returns the instance
 */
public class UserDatabase implements interfaces.UserDatabase {

    private LinkedList<User> list;
    private static UserDatabase instance = null;

    public static UserDatabase getInstance(){
        if(instance == null){
            instance = new UserDatabase();
        }
        return instance;
    }

    private UserDatabase(){
        list = new LinkedList<>();
    }

    public void add(User user){
        list.add(user);
    }

    public Optional<User> findUser(final String searchTerm){
        return list.stream().filter(c -> c.getCpr().equals(searchTerm)).findAny();
    }

    public void removeUser(final String searchTerm){
        list.removeIf(p -> (findUser(searchTerm).isPresent()));
    }
}
