package rest;

import code.Customer;
import database.UserDatabase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Path("/addCustomers")
public class AddCustomerEndpoint {

    UserDatabase db = UserDatabase.getInstance();

    /**
     * Adds customer to database
     * @author Alen
     * @param body
     * @return Response.accepted("Added Customer")
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)

    public Response addCustomer(AddBody body){
        Customer customer = new Customer(body.name, body.cpr);
        db.add(customer);
        return Response.accepted("Added Customer.").build();
    }
}

@XmlRootElement
class AddBody implements Serializable {
    @XmlElement String cpr;
    @XmlElement String name;
}

