package rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import code.*;
import com.google.gson.Gson;
import database.UserDatabase;
import sun.security.provider.certpath.OCSPResponse;

import java.io.Serializable;
import java.util.Optional;

@Path("/getCustomers/{cpr}")
public class GetCustomersEndpoint {
    UserDatabase db = UserDatabase.getInstance();

    /**
     * Gets customer as a Json object with status code OK
     * Author: Erik
     * @param cpr
     * @return Response.status.ok : 200
     */
    @GET
    //@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@PathParam("cpr") String cpr) {
        Optional<User> customer = db.findUser(cpr);
        if(!customer.isPresent() || !(customer.get() instanceof Customer)) return Response.status(Response.Status.NOT_FOUND).build();
        Gson gson = new Gson();
        AddCustomerBody body = new AddCustomerBody();
        body.cpr = customer.get().getCpr();
        body.name = customer.get().getName();
        String cust = gson.toJson(body);
        return Response.status(Response.Status.OK).entity(cust).build();
    }

    /**
     * Response for when customer is added to database.
     * Author: Erik
     * @param addCustomerBody
     * @return Response.accepted("Added Customer.")
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCustomer(AddCustomerBody addCustomerBody){
        Customer customer = new Customer(addCustomerBody.name, addCustomerBody.cpr);
        db.add(customer);
        return Response.accepted("Added Customer.").build();
    }
}

@XmlRootElement
class AddCustomerBody implements Serializable {
    @XmlElement String cpr;
    @XmlElement String name;
}
