package code;

public class Merchant extends User {

    /**
     * @author Frederik
     * @param name
     * @param cpr
     */
    public Merchant(String name, String cpr) {
        super(name, cpr);
    }
}
